<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KalkulatorModel;

class KalkulatorController extends Controller
{
    public function store(Request $request)
    {
        $kal = $request->all();
        KalkulatorModel::setKal($kal);

        return redirect('/')->with('hasil', KalkulatorModel::getHasil());
    }
}
