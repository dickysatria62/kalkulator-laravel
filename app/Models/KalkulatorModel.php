<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KalkulatorModel extends Model
{
    use HasFactory;

    private static $output = 0;
    private static $kal = ["a" => 0, "b" => 0, "operator" => '+'];

    public static function setKal($operator)
    {
        self::$kal = $operator;
    }

    public static function setHitung()
    {
        $a = (int)self::$kal['a'];
        $b = (int)self::$kal['b'];
        $operator = self::$kal['operator'];
        switch ($operator){
            case '+' :
                $hasil = $a + $b;
                break;
            case '-';
                $hasil = $a - $b;
                break;
            case '*';
                $hasil = $a * $b;
                break;
            case '/';
                if($b !== 0){
                    $hasil = $a / $b;
                }else{
                    $hasil = "Tidak bisa dilakukan";
                }
                break;
            default:
            $hasil = "";
        }
        return $hasil;
    }

    public static function getHasil()
    {
        return self::$output = KalkulatorModel::setHitung();
    }
}
