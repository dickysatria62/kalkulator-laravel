@extends('layout.head')
@section('content')

	<div class="kalkulator">
		<h2 class="judul">KALKULATOR</h2>
		<form method="post" action="/">
            @csrf
			<input type="text" name="a" class="bil" autocomplete="off" placeholder="Masukkan Bilangan Pertama">
			</br>
			</br>
			<input type="text" name="b" class="bil" autocomplete="off" placeholder="Masukkan Bilangan Kedua">
			</br>
			</br>
			<select class="opt" name="operator">
				<option value="+">+</option>
				<option value="-">-</option>
				<option value="*">x</option>
				<option value="/">/</option>
			</select>
			<input type="submit" name="operasi" class="tombol">
		</form>
		</br>

        @if (session()->has('hasil'))
            <input type="text" value="{{ session('hasil') }}" class="bil">
        @endif
	</div>
    @endsection
